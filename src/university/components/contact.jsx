/* eslint-disable react/button-has-type */
import React from 'react';
import Link from 'next/link';
import UniversityUtils from 'utils/university';
import styles from '../styles.scss';

class ContactComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    return (
      <React.Fragment>
        {this.labelInternational()}
        <h2>{this.university.name}</h2>
        {this.accredited()}
        {this.contact()}
      </React.Fragment>
    );
  }

  labelInternational() {
    const { isInternational } = this.university;
    if (!isInternational) return null;
    return <div className={styles.international}>International</div>;
  }

  accredited() {
    return (
      <div className={styles.accredited}>
        <h4>{`Accredited ${this.university.accredited}`}</h4>
        {this.ratings()}
      </div>
    );
  }

  ratings() {
    const { university } = this;
    const ratingRounded = university.average_rating ? Math.round(university.average_rating) : 0;
    if (ratingRounded === 0) return null;

    return (
      <div className={styles.stars_rating}>
        {
          [...Array(ratingRounded)].map((e, i) => <img src="/static/icons/icon-star-review.svg" alt="rating" key={`st-${i}`} />)
        }
        <span>{university.average_rating}</span>
      </div>
    );
  }

  contact() {
    const { university } = this;
    const { city } = university;

    if (!city || !city.province) return null;

    const { province } = city;
    const completeAddress = `${university.address}, ${university.sub_district}, ${city.name}, ${province.name} `;
    const completePhone = `${university.phone}, ${university.phone_2}`;
    const expenseObj = UniversityUtils.getExpenseStatus(university);
    return (
      <div className={styles.contact_detail}>
        <div className={styles.contact_row}>
          <div className={styles.contact_icon}>
            <img src="/static/icons/icon-address.svg" alt="address" />
          </div>
          {completeAddress || '-'}
        </div>
        <div className={styles.contact_row}>
          <div className={styles.contact_icon}>
            <img src="/static/icons/icon-phone.svg" alt="address" />
          </div>
          {completePhone || '-'}
        </div>
        <div className={styles.contact_row}>
          <div className={styles.contact_icon}>
            <img src="/static/icons/icon-website.svg" alt="address" />
          </div>
          {university.website}
        </div>
        <div className={styles.contact_row}>
          <div className={styles.contact_icon}>
            <img src="/static/icons/icon-expense.svg" alt="address" width="8px" />
          </div>
          {expenseObj.label}
        </div>
        {this.announcement()}
        {this.otherCampuses()}
      </div>
    );
  }

  announcement() {
    if (!this.university.announcement) return null;
    return (
      <div className={styles.announcement}>
        <div className={styles.announce_img}>
          <img src="/static/icons/icon-announcement.svg" alt="announcement" />
        </div>
        {this.university.announcement}
      </div>
    );
  }

  otherCampuses() {
    return (
      <div className={styles.others}>
        <Link href="">
          <React.Fragment>
            <a>View other campuses’ locations (4)</a>
            <img src="/static/icons/icon-arrow-right.svg" alt="other campuses" />
          </React.Fragment>
        </Link>
      </div>
    );
  }
}

export default props => new ContactComponent(props).root();
