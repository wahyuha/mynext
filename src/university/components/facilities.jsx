/* eslint-disable react/button-has-type */
import React from 'react';
import styles from '../styles.scss';

class FacilitiesComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    const { facilities } = this.university;
    if (!facilities) return null;
    return (
      <React.Fragment>
        <div className={styles.facilities}>
          <h3>Facilities</h3>
          <ul>{facilities.map(facility => <li key={`ss-${facility.id}`}>{facility.name}</li>)}</ul>
        </div>
        <div className={styles.borders} />
      </React.Fragment>
    );
  }
}

export default props => new FacilitiesComponent(props).root();
