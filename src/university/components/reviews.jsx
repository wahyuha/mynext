/* eslint-disable react/button-has-type */
import React from 'react';
import Link from 'next/link';
import dayjs from 'dayjs';
import UDPConstructor from './base.js';
import styles from '../styles.scss';

class ReviewsComponent extends UDPConstructor {
  root() {
    const { feedbacks: { count } } = this;
    if (!this.feedbacks || count <= 0) return null;

    return (
      <div className={styles.reviews}>
        <h3>Review</h3>
        {this.ratings()}
        <h4>{`Average rating from ${count} reviews`}</h4>
        <div className={styles.input_review}>
          <img src="/static/reviews/icon-review.svg" alt="review" />
          <input type="text" placeholder="Add your review…" />
        </div>
        {this.reviewList()}
      </div>
    );
  }

  ratings() {
    const { university } = this;
    const ratingRounded = university.average_rating ? Math.round(university.average_rating) : 0;
    if (ratingRounded === 0) return null;

    return (
      <div className={styles.stars_rating}>
        {
          [...Array(ratingRounded)].map((e, i) => <img src="/static/icons/icon-star-review.svg" alt="rating" key={`st-${i}`} />)
        }
        <span>{university.average_rating}</span>
      </div>
    );
  }

  reviewList() {
    if (!this.feedbacks) return null;

    const { results, count } = this.feedbacks;
    return (
      <div className={styles.review_list}>
        {results.map(feedback => this.reviewRow(feedback))}
        {this.otherReviews(count)}
      </div>
    );
  }

  reviewRow(feedback) {
    const formattedDate = dayjs(feedback.created_at).format('MM-DD mm:ss');
    return (
      <div className={styles.review_row}>
        <img src="/static/reviews/icon-user.png" alt="user" />
        <div className={styles.review_wrap}>
          <div className={styles.username}>Wahyu</div>
          <div className={styles.review_rating}>
            <span className={styles.rating_number}>{feedback.rating}</span>
            <span>{formattedDate}</span>
          </div>
          <div className={styles.review_content}>
            {feedback.comment}
          </div>
        </div>
      </div>
    );
  }

  otherReviews(count) {
    return (
      <div className={styles.others}>
        <Link href="">
          <React.Fragment>
            <a>{`View all reviews (${count})`}</a>
            <img src="/static/icons/icon-arrow-right.svg" alt="other faculties" />
          </React.Fragment>
        </Link>
      </div>
    );
  }
}

export default props => new ReviewsComponent(props).root();
