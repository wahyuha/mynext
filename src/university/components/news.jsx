/* eslint-disable react/button-has-type */
import React from 'react';
import NewsItem from 'src/common/news';
import styles from '../styles.scss';

class NewsComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    const { news } = this.university;
    if (news.length <= 0) return null;
    return (
      <React.Fragment>
        <div className={styles.scholar}>
          <h3>Campus Updates</h3>
          {news.map(scholarship => <NewsItem {...scholarship} />)}
        </div>
        <div className={styles.borders} />
      </React.Fragment>
    );
  }
}

export default props => new NewsComponent(props).root();
