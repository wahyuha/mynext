/* eslint-disable react/button-has-type */
import React from 'react';
import ContactComponent from './contact';
import FacilitiesComponent from './facilities';
import FacultiesComponent from './faculties';
import GalleriesComponent from './galleries';
// import ReviewsComponent from './reviews';
import ScholarshipsComponent from './scholarships';
import NewsComponent from './news';
import ThumbnailComponent from './thumbnail';
import styles from '../styles.scss';

class UDPComponent {
  constructor(data) {
    const { universities } = data;
    [this.university] = universities;
  }

  root() {
    return (
      <div className={styles.detail_university}>
        <ThumbnailComponent university={this.university} />
        <div className={styles.content_info}>
          <ContactComponent university={this.university} />
          <div className={styles.borders} />
          <FacultiesComponent university={this.university} />
          <FacilitiesComponent university={this.university} />
          <ScholarshipsComponent university={this.university} />
          <NewsComponent university={this.university} />
          {
            this.university.images.length > 0
            && (
              <React.Fragment>
                <GalleriesComponent cover={this.university.cover} images={this.university.images} />
                <div className={styles.borders} />
              </React.Fragment>
            )
          }
          {/* <ReviewsComponent {...this.props} /> */}
        </div>
      </div>
    );
  }
}

export default props => new UDPComponent(props).root();
