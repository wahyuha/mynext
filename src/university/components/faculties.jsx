/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import UniversityUtils from 'utils/university';
import ButtonTabs from 'src/common/buttonTabs/component';
import styles from '../styles.scss';

class FacultiesComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    const { faculty_degree: faculties } = this.university;
    const grouped = UniversityUtils.facultiesByDegree(faculties);
    const [selectedGroup, setSelectedGroup] = useState('ALL');

    if (grouped[selectedGroup].length === 0) return null;
    return (
      <React.Fragment>
        <div className={styles.faculties}>
          <h3>Faculties & Programs</h3>
          {this.tabs({ grouped, selectedGroup, setSelectedGroup })}
          {this.facultyCategories(grouped[selectedGroup])}
          {/* {this.otherFaculties()} */}
        </div>
        <div className={styles.borders} />
      </React.Fragment>
    );
  }

  tabs({ grouped, selectedGroup, setSelectedGroup }) {
    const options = [];
    Object.keys(grouped).map(key => options.push({
      label: key,
      value: key,
      selected: key === selectedGroup
    }));
    return (
      <ButtonTabs
        options={options}
        onSelect={selected => setSelectedGroup(selected)}
      />
    );
  }

  facultyCategories(faculties) {
    const facultiesGroup = UniversityUtils.facultiesByParent(faculties);
    return (
      <div className={styles.fac_categories}>
        {Object.keys(facultiesGroup).map(group => this.groupFaculties(group, facultiesGroup[group]))}
      </div>
    );
  }

  groupFaculties(group, facultiesGroup) {
    return (
      <React.Fragment>
        <h4>{group}</h4>
        <ul>
          {facultiesGroup.map(child => <li key={`fac-${child.faculty.id}`}>{child.faculty.name}</li>)}
        </ul>
      </React.Fragment>
    );
  }

  otherFaculties() {
    return (
      <div className={styles.others}>
        <Link href="">
          <React.Fragment>
            <a>View all faculties & programs (14)</a>
            <img src="/static/icons/icon-arrow-right.svg" alt="other faculties" />
          </React.Fragment>
        </Link>
      </div>
    );
  }
}

export default props => new FacultiesComponent(props).root();
