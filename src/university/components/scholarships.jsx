/* eslint-disable react/button-has-type */
import React from 'react';
import ScholarshipsItem from 'src/common/scholarship';
import styles from '../styles.scss';

class ScholarshipComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    const { scholarships } = this.university;
    if (scholarships.length <= 0) return null;
    return (
      <React.Fragment>
        <div className={styles.scholar}>
          <h3>Scholarship Program</h3>
          {scholarships.map(scholarship => <ScholarshipsItem {...scholarship} />)}
        </div>
        <div className={styles.borders} />
      </React.Fragment>
    );
  }
}

export default props => new ScholarshipComponent(props).root();
