/* eslint-disable react/button-has-type */
import React from 'react';
import Carousel, { Modal, ModalGateway } from 'react-images';
import styles from '../styles.scss';

export default class GalleriesComponent extends React.Component {
  constructor(props) {
    super(props);
    const universityimages = props.images;
    const coverProps = {
      id: 1,
      position: 0,
      image: props.cover
    };
    const injectedImages = [coverProps];
    universityimages.forEach((image, index) => {
      injectedImages.push({
        ...image,
        src: image.url,
        position: image.position || index
      });
    });
    this.state = {
      images: injectedImages,
      selectedIndex: 0,
      lightboxIsOpen: false
    };
  }

  toggleLightbox = (imageObject) => {
    this.setState(state => ({
      lightboxIsOpen: !state.lightboxIsOpen,
      selectedIndex: imageObject.position
    }));
  };


  slideShow() {
    const { images, lightboxIsOpen, selectedIndex } = this.state;
    const formattedImages = [];
    images.forEach((gal) => {
      const src = gal.image.url;
      formattedImages.push({ src });
    });
    if (!lightboxIsOpen) return null;

    return (
      <React.Fragment>
        <ModalGateway>
          <Modal onClose={this.toggleLightbox}>
            <Carousel
              components={<div>footer</div>}
              currentIndex={selectedIndex}
              views={formattedImages}
            />
          </Modal>
        </ModalGateway>
      </React.Fragment>
    );
  }

  render() {
    const { cover } = this.props;
    const { images } = this.state;
    images && images.shift();

    return (
      <React.Fragment>
        {this.slideShow()}
        <div className={styles.album}>
          <h3>Photo Album</h3>
          <div className={styles.galleries}>
            <div className={styles.thumbnail}>
              <img src={cover.url} alt="" onClick={() => this.toggleLightbox({ ...cover, position: 0 })} />
            </div>
            <div className={styles.gallery}>
              {images.map((gallery) => {
                const { image } = gallery;
                return (
                  <div className={styles.gallery_item} key={`imgs-${image.id}`}>
                    <img src={image.url} alt="" onClick={() => this.toggleLightbox(gallery)} />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
