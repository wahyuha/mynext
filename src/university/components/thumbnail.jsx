/* eslint-disable react/button-has-type */
import React from 'react';
import Router from 'next/router';
import styles from '../styles.scss';

class ThumbnailComponent {
  constructor(props) {
    this.university = props.university;
  }

  root() {
    const { cover } = this.university;
    const imageFile = cover ? cover.url : null;
    return (
      <div className={styles.cover}>
        {imageFile ? <img src={imageFile} alt={this.university.name} /> : ''}
        {this.header()}
      </div>
    );
  }

  header() {
    return (
      <div className={styles.header}>
        <button
          className={styles.icons}
          onClick={() => Router.back()}
          onKeyPress={() => Router.back()}
        >
          <img src="/static/icons/icon-back-white.svg" alt="Back" />
        </button>
        <button
          className={styles.icons}
        >
          <img src="/static/icons/icon-share.svg" alt="Back" />
        </button>
      </div>
    );
  }
}

export default props => new ThumbnailComponent(props).root();
