import { useQuery } from '@apollo/react-hooks';
import { LoaderFull } from 'src/common/loader';
import React from 'react';
import UniversityDetailComponent from './components/main';
import MainWrapper from '../common/wrapper';
import UNIVERSITY_DETAIL from './query';

const University = (props) => {
  const { loading, error, data } = useQuery(UNIVERSITY_DETAIL, {
    variables: { where: { id: props.slug } },
    notifyOnNetworkStatusChange: true
  });
  if (loading) return LoaderFull('Fetching detail ..');
  if (data) {
    return (
      <MainWrapper>
        {UniversityDetailComponent(data)}
      </MainWrapper>
    );
  }
  if (error) return 'error occured';
  return null;
};

University.getInitialProps = (ctx) => {
  const { query } = ctx;
  const { slug } = query;
  return { slug };
};

export default University;
