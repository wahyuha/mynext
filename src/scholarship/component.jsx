import React from 'react';
import ReactMarkdown from 'react-markdown/with-html';
import TopNavbar from 'src/common/topNavbar';
import styles from './styles.scss';

class ScholarshipComponent {
  constructor(props) {
    this.props = props;
    this.detail = props.scholarshipState.detail.data;
  }

  render() {
    return (
      <div className={styles.detail_scho}>
        <TopNavbar />
        {this.cover()}
        <div className={styles.content_scho}>
          {this.title()}
          {this.content()}
        </div>
      </div>
    );
  }

  cover() {
    const { image } = this.detail;
    return (
      <div className={styles.cover}>
        <img src={image.url} alt={this.detail.title} />
      </div>
    );
  }

  title() {
    return (
      <div className={styles.title}>
        <h1>
          {`SCHOLARSHIP • ${this.detail.title}`}
        </h1>
      </div>
    );
  }

  content() {
    return (
      <div className={styles.content}>
        <ReactMarkdown source={this.detail.description} escapeHtml={false} />
      </div>
    );
  }
}

const Component = props => new ScholarshipComponent(props).render();

export default Component;
