import { connect } from 'react-redux';
import { LoaderFull } from 'src/common/loader';
import PropTypes from 'prop-types';
import React from 'react';
import ScholarshipReceiver from 'utils/scholarship/receiver';
import ScholarshipDetailComponent from './component';
import MainWrapper from '../common/wrapper';

class Scholarship extends React.Component {
  static async getInitialProps(context) {
    const { reduxStore, query, req } = context;
    const { slug } = query;
    let loaded = false;
    if (req && slug) {
      loaded = true;
      const { dispatch } = reduxStore;
      await ScholarshipReceiver.getScholarshipDetail.bind({ dispatch })(slug);
    }
    return { slug, loaded };
  }

  componentDidMount() {
    if (this.props.loaded) return;
    const { dispatch, slug, scholarshipState } = this.props;
    ScholarshipReceiver.getScholarshipDetail.bind({ dispatch, scholarshipState })(slug);
  }

  render() {
    const { scholarshipState: { detail: { status } } } = this.props;

    if (status === 'fetched') {
      return (
        <MainWrapper>
          {ScholarshipDetailComponent({ ...this.props })}
        </MainWrapper>
      );
    }
    return LoaderFull('Fetching detail ..');
  }
}

const mapStateToProps = state => ({
  scholarshipState: state.scholarshipReducer
});

Scholarship.propTypes = {
  dispatch: PropTypes.func.isRequired,
  slug: PropTypes.string.isRequired,
  scholarshipState: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(Scholarship);
