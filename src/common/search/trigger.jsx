import styles from './styles.scss';

class SearchInputTrigger {
  static render(props) {
    return new SearchInputTrigger(props).root();
  }

  constructor(props) {
    this.props = props;
  }

  root() {
    return (
      <div className={styles.search_section}>
        <div className={styles.input_section}>
          <i />
          <input
            type="text"
            placeholder="Search for school…"
            name="searchInput"
            onFocus={this.props.onFocus}
          />
        </div>
        {this.props.disableFilter ? null : (
          <button
            className={styles.button_filter}
            type="button"
            // disabled={this.props.disabled}
            onClick={this.props.sortFilter}
          />
        )}
      </div>
    );
  }
}

export default SearchInputTrigger.render;
