import styles from './styles.scss';

class SearchCounter {
  static render(props) {
    return new SearchCounter(props).root();
  }

  constructor(props) {
    this.props = props;
  }

  root() {
    return (
      <div className={styles.counter}>
        <h5>{`Showing ${this.props.count} results`}</h5>
      </div>
    );
  }
}

export default SearchCounter.render;
