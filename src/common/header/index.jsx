import React from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import IconBack from './assets/icon_back.svg';
import styles from './styles.scss';

class Header extends React.Component {
  render() {
    return (
      <header className={styles.header}>
        <a
          tabIndex="0"
          role="button"
          className={styles.icon_navigator}
          onClick={this.handleBack}
          onKeyPress={this.handleBack}
        >
          <img src={IconBack} alt="back" />
        </a>
        {this.renderComponent()}
      </header>
    );
  }

  renderComponent() {
    const { title, children } = this.props;
    const titleLabel = title || '';
    if (Object.keys(children).length) return children;
    return <h3>{titleLabel}</h3>;
  }

  handleBack() {
    return Router.back();
  }
}

Header.propTypes = {
  title: PropTypes.string,
  children: PropTypes.object
};

Header.defaultProps = {
  title: '',
  children: {}
};

const Component = Header;

export { Component as Header };
