import Link from 'next/link';
import React from 'react';
import UniversityUtils from 'utils/university';
import styles from './styles.scss';
import { ReactReduxContext } from 'react-redux';

class ListUniversitiesComponent {
  static components(props) {
    return new ListUniversitiesComponent(props).root();
  }

  constructor(props) {
    this.props = props;
  }

  root() {
    return (
      <div className={styles.list_wrapper}>
        {this.props.list.map(university => this.itemUniversity(university))}
      </div>
    );
  }

  getFaculties(university) {
    const { faculties } = university;
    const listFaculties = { facultiesString: '', hasMore: '' };
    if (!faculties) return listFaculties;

    const concatedFaculties = faculties.map((faculty) => {
      const concatedNames = '';
      return concatedNames.concat(faculty.name);
    });

    listFaculties.facultiesString = concatedFaculties.slice(0, 4).join(', ');
    listFaculties.hasMore = concatedFaculties.length >= 3;

    return listFaculties;
  }

  itemCity(university) {
    // TODO: combine with province
    const { city } = university;
    if (!city) return null;
    return university.city.name;
  }

  itemUniversity(university) {
    const { cover, accredited } = university;
    const { facultiesString, hasMore } = this.getFaculties(university);
    return (
      <Link
        href={`/university?slug=${university.id}`}
        as={`/university/${university.id}`}
        key={`un-${university.id}`}
      >
        <a href={`/university/${university.id}`} className={styles.ahref}>
          <div className={styles.item_university}>
            <div className={styles.uni_info}>
              {this.labelInternational(university)}
              <h2>{university.name}</h2>
              <div className={styles.list_city}>{this.itemCity(university)}</div>
              <div className={styles.list_accr}>
                {`${accredited}-Accredited  •`}
                {this.expense(university)}
              </div>
              <div className={styles.list_facult}>{`${facultiesString} ${hasMore ? 'and more' : ''}`}</div>
              {hasMore && this.seeAll(university)}
            </div>
            <div className={styles.uni_image}>
              <img src={cover.url} alt={university.name} />
            </div>
          </div>
        </a>
      </Link>
    );
  }

  labelInternational(university) {
    const { isInternational } = university;
    if (!isInternational) return null;
    return <div className={styles.international}>International</div>;
  }

  seeAll(university) {
    const { faculties } = university;
    const facultiesCount = faculties.length;
    if (facultiesCount) {
      return (
        <div className={styles.see_all}>
          <b>See all: </b>
          <span>{`Faculties and Programs (${facultiesCount})`}</span>
        </div>
      );
    }
    return null;
  }

  expense(university) {
    const { expense } = university;
    if (!expense) return null;

    const expenseObj = UniversityUtils.getExpenseStatus(university);

    return (
      <div className={styles.expense}>
        {[...Array(expenseObj.number)].map((v, i) => <img src="/static/icon-expense.svg" alt="expense" key={`exs-${i}`} />)}
      </div>
    );
  }
}

export default ListUniversitiesComponent.components;
