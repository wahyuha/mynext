import React from 'react';
import Link from 'next/link';
import styles from './styles.scss';

const listScholarship = (props) => {
  const { image } = props;
  return (
    <Link
      href={`/scholarship?slug=${props.id}`}
      as={`/scholarship/${props.id}`}
      key={`scho-${props.id}`}
    >
      <div className={styles.scholar_row}>
        <div className={styles.scholar_img}>
          {image && <img src={image.url} alt={props.title} />}
        </div>
        {props.title}
      </div>
    </Link>
  );
};

export default listScholarship;
