import React from 'react';
import Head from 'next/head';
import styles from './styles.scss';

class Wrapper extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className={styles.app_container}>
        <Head>
          <title>Student</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" key="viewport" />
        </Head>
        {this.props.children}
      </div>
    );
  }
}

export default Wrapper;
