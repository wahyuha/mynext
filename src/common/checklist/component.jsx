import React from 'react';
import styles from './styles.scss';

class Checklist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      options: props.options
    };
  }

  selectThis(value) {
    const clonedState = Object.assign([], this.state.options);
    const newState = clonedState.map((option) => {
      const { selected } = option;
      return ({
        ...option,
        selected: option.value === value ? !selected : selected
      });
    });
    this.setState({ options: newState });
    this.props.onSelect(newState);
  }

  itemChecklist({ value, label, selected }) {
    return (
      <li key={`chk-${value}`}>
        <label className={styles.check_control}>
          {label}
          <input type="checkbox" id={value} checked={selected} onChange={() => this.selectThis.bind(this)(value)} />
          <span className={styles.checkmark} />
        </label>
      </li>
    );
  }

  render() {
    return (
      <div className={styles.more_filters}>
        <ul className={styles.options}>
          {this.state.options.map(option => this.itemChecklist(option))}
        </ul>
      </div>
    );
  }
}

export default Checklist;
