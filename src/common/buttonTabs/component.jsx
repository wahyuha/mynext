import React from 'react';
import cx from 'classnames';
import styles from './styles.scss';

class ButtonTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      options: props.options
    };
  }

  selectThis(value) {
    this.setState((prevState) => {
      return prevState.options.map((option) => {
        delete option.selected;
        const newOptions = option;
        if (option.value === value) newOptions.selected = true;

        return newOptions;
      });
    });
    this.props.onSelect && this.props.onSelect(value);
  }

  buttonTabs({ value, label, selected }) {
    const isActive = cx({
      [styles.active]: selected
    });
    return (
      // eslint-disable-next-line react/button-has-type
      <button
        className={isActive}
        onClick={() => this.selectThis.bind(this)(value)}
        key={`btabs-${value}`}
      >
        {label}
      </button>
    );
  }

  render() {
    return (
      <div className={styles.fac_tabs}>
        {this.state.options.map(option => this.buttonTabs(option))}
      </div>
    );
  }
}

export default ButtonTabs;
