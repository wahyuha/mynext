import React from 'react';
import classNames from 'classnames';
import styles from './styles.scss';

export default class Dropdowns extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      label: this.isSelected()
    };
  }

  isSelected() {
    const selectedObj = this.props.options.find(option => option.selected);
    if (selectedObj) {
      return selectedObj.label;
    }
    return '';
  }

  selectThis(value, label) {
    this.setState({
      expand: false,
      label
    });
    this.props.onSelect(value);
  }

  render() {
    const classesObj = this.props.className
      ? Object.assign({[this.props.className]: true}, {[styles.dropdown]: true})
      : {[styles.dropdown]: true};
    const classes = classNames(classesObj);
    return (
      <div className={classes}>
        {this.labelView()}
        {this.state.expand && this.selections()}
      </div>
    );
  }

  labelView() {
    return (
      <div className={styles.dd_default} onClick={() => this.setState({ expand: true })}>
        <span className={styles.dd_label}>{this.state.label}</span>
        <i className={styles.dd_icon_down} />
      </div>
    );
  }

  selections() {
    return (
      <ul className={styles.dd_selection}>
        {this.props.options.map((option) => {
          const { value, label } = option;
          return <li onClick={() => this.selectThis.bind(this)(value, label) }>{label}</li>
        })}
      </ul>
    );
  }
}
