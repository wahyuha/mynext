import styles from './styles.scss';

class Loader {
  static dots() {
    return (
      <div className={styles.spinner}>
        <div className={styles.bounce1} />
        <div className={styles.bounce2} />
        <div className={styles.bounce3} />
      </div>
    );
  }

  static full(info) {
    const title = info || 'Please wait';
    return (
      <div className={styles.loader_modal}>
        <div className={styles.loader_content}>
          <h2>{title}</h2>
          {Loader.dots()}
        </div>
      </div>
    );
  }
}

const full = info => Loader.full(info);
const dots = () => Loader.dots();

export { dots as DotsColorful, full as LoaderFull };
