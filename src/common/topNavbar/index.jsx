import React from 'react';
import Router from 'next/router';
import SearchInputTrigger from 'src/common/search/trigger';
import styles from './styles.scss';

const navbar = (props) => {
  return (
    <div className={styles.home_header}>
      <div className={styles.menu}>
        <div />
        <div />
        <div />
      </div>
      <div className={styles.search_wrap}>
        <SearchInputTrigger
          className={styles.ss}
          onFocus={() => Router.push('/search', { advanced: true })}
          disableFilter={true}
        />
      </div>
    </div>
  );
};

export default navbar;
