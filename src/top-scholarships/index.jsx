import { connect } from 'react-redux';
import { LoaderFull } from 'src/common/loader';
import PropTypes from 'prop-types';
import React from 'react';
import ScholarshipReceiver from 'utils/scholarship/receiver';
import ScholarshipsComponent from './component';
import MainWrapper from '../common/wrapper';

class Scholarships extends React.Component {
  static async getInitialProps(context) {
    const { reduxStore, query, req } = context;
    const { category } = query;
    let loaded = false;
    if (req) {
      loaded = true;
      const { dispatch } = reduxStore;
      await ScholarshipReceiver.getScholarships.bind({ dispatch })();
    }
    return { category, loaded };
  }

  componentDidMount() {
    if (this.props.loaded) return;
    const { dispatch } = this.props;
    ScholarshipReceiver.getScholarships.bind({ dispatch })();
  }

  render() {
    const { scholarshipState: { status } } = this.props;

    if (status === 'fetched') {
      return (
        <MainWrapper>
          {ScholarshipsComponent({ ...this.props })}
        </MainWrapper>
      );
    }
    return LoaderFull('Fetching detail ..');
  }
}

const mapStateToProps = state => ({
  scholarshipState: state.scholarshipReducer
});

Scholarships.propTypes = {
  dispatch: PropTypes.func.isRequired,
  slug: PropTypes.string.isRequired,
  scholarshipState: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(Scholarships);
