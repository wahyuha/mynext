import React from 'react';
import Router from 'next/router';
import TopNavbar from 'src/common/topNavbar';
import { Header } from 'src/common/header';
import SearchInputTrigger from 'src/common/search/trigger';
import ScholarshipsItem from 'src/common/scholarship';
import styles from './styles.scss';

class ScholarshipsComponent {
  constructor(props) {
    this.props = props;
  }

  render() {
    return (
      <div className={styles.detail_scho}>
        <Header title="Top Scholarship" />
        <div className={styles.main_board_wrap}>
          <SearchInputTrigger
            onFocus={() => Router.push('/search')}
          />
        </div>
        {this.listScholarships()}
      </div>
    );
  }

  listScholarships() {
    const { scholarshipState: { scholarships } } = this.props;
    return (
      <div className={styles.content}>
        <div className={styles.scholar}>
          <div className={styles.scholar_header}>
            <h3>Scholarship Program</h3>
          </div>
          {scholarships.map(scho => <ScholarshipsItem {...scho} />)}
        </div>
      </div>
    );
  }
}

const Component = props => new ScholarshipsComponent(props).render();

export default Component;
