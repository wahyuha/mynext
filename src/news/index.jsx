import { connect } from 'react-redux';
import { LoaderFull } from 'src/common/loader';
import PropTypes from 'prop-types';
import React from 'react';
import NewsReceiver from 'utils/news/receiver';
import NewsDetailComponent from './component';
import MainWrapper from '../common/wrapper';

class News extends React.Component {
  static async getInitialProps(context) {
    const { reduxStore, query, req } = context;
    const { slug } = query;
    let loaded = false;
    if (req && slug) {
      loaded = true;
      const { dispatch } = reduxStore;
      await NewsReceiver.getNewsDetail.bind({ dispatch })(slug);
    }
    return { slug, loaded };
  }

  componentDidMount() {
    if (this.props.loaded) return;
    const { dispatch, slug } = this.props;
    NewsReceiver.getNewsDetail.bind({ dispatch })(slug);
  }

  render() {
    const { newsState: { detail: { status } } } = this.props;

    if (status === 'fetched') {
      return (
        <MainWrapper>
          {NewsDetailComponent({ ...this.props })}
        </MainWrapper>
      );
    }
    return LoaderFull('Fetching detail ..');
  }
}

const mapStateToProps = state => ({
  newsState: state.newsReducer
});

News.propTypes = {
  dispatch: PropTypes.func.isRequired,
  slug: PropTypes.string.isRequired,
  newsState: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(News);
