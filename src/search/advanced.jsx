/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/button-has-type */
import React from 'react';
import cx from 'classnames';
import Router from 'next/router';
// import InputRange from 'react-input-range';
import ButtonTabs from '../common/buttonTabs/component';
import Checklist from '../common/checklist/component';
import DropDown from '../common/dropdown/component';
import styles from './styles.scss';

class Advanced {
  static components(prop) {
    return new Advanced(prop).root();
  }

  constructor(props) {
    this.props = props;
  }

  root() {
    const { params: { selector } } = this.props;
    return (
      <div className={styles.advanced}>
        <div className={styles.pop_block} onClick={this.props.functions.openAdvanced} />
        <div className={styles.adv_content}>
          {this.header()}
          <div className={styles.content_wrap}>
            {selector ? this.selector(selector) : this.mainAdvanced()}
            {this.applyButton()}
          </div>
        </div>
      </div>
    );
  }

  header() {
    return (
      <div className={styles.fs_header}>
        <h3>Sort & Filter</h3>
        <a>Clear All</a>
      </div>
    );
  }

  selector(type) {
    const { searchState: { filter: { locations, faculties } } } = this.props;
    if (!locations || !faculties) return null; // TODO: put status on filter to have loader
    let title = 'Filter by Faculties & Programs';
    let selectFunction = options => this.props.functions.setFaculties(options);
    if (type === 'loc') {
      title = 'Locations';
      selectFunction = options => this.props.functions.setLocations(options);
    }

    return (
      <React.Fragment>
        <div className={styles.selector_header}>
          <button
            onClick={() => Router.back()}
            onKeyPress={() => Router.back()}
          >
            <img src="/static/icons/icon-arrow-left.svg" alt="back" />
          </button>
          <h2>{title}</h2>
        </div>
        <Checklist
          options={type === 'loc' ? locations : faculties}
          onSelect={selectFunction}
        />
      </React.Fragment>
    );
  }

  mainAdvanced() {
    return (
      <React.Fragment>
        {this.sortBy()}
        {this.expenseLevel()}
        {this.rating()}
        {this.locations()}
        {this.faculties()}
        {this.moreFilters()}
      </React.Fragment>
    );
  }

  sortBy() {
    const options = [
      {
        label: 'Popularity',
        value: 'popularity',
        selected: true
      },
      {
        label: 'Rating',
        value: 'rating'
      },
      {
        label: 'Name A-Z',
        value: 'name_asc'
      },
      {
        label: 'Name Z-A',
        value: 'name_desc'
      }
    ];
    return (
      <div className={styles.fs_sort}>
        <h4>Sort by</h4>
        <DropDown
          className={cx(styles.dropdown)}
          options={options}
          onSelect={value => this.props.functions.setSortingValue(value)}
        />
      </div>
    );
  }

  expenseLevel() {
    const options = [
      {
        label: 'All',
        value: 'All',
        selected: true
      },
      {
        label: '< 1000K',
        value: '< 1000000'
      },
      {
        label: '1000K - 2000K',
        value: '1000000 - 2000000'
      },
      {
        label: '> 2000K',
        value: '> 2000000'
      }
    ];
    return (
      <div className={styles.expense}>
        <h4>Education Fees</h4>
        <ButtonTabs
          options={options}
          onSelect={value => this.props.functions.setFilterValues({ fee: value })}
        />
      </div>
    );
  }

  rating() {
    const options = [
      {
        label: this.generateStar(1),
        value: '1',
        selected: true
      },
      {
        label: this.generateStar(2),
        value: '2'
      },
      {
        label: this.generateStar(3),
        value: '3'
      },
      {
        label: this.generateStar(4),
        value: '4'
      },
      {
        label: this.generateStar(5),
        value: '5'
      }
    ];
    return (
      <div className={styles.fs_sort}>
        <h4>University Rating</h4>
        <DropDown
          className={cx(styles.rating_dd)}
          options={options}
          onSelect={value => this.props.functions.setFilterValues({ rating: value })}
        />
      </div>
    );
  }

  generateStar(n) {
    return (
      <div className={styles.stars_rating}>
        {
          [...Array(n)].map((e, i) => <img src="/static/icons/icon-star-review.svg" alt="rating" key={`st-${i}`} />)
        }
      </div>
    );
  }

  locations() {
    return (
      <div
        className={cx(styles.faculties, styles.trigger_right)}
        onClick={() => this.props.functions.setSelector('loc')}
      >
        University Locations
        <i className={styles.chevron_right} />
      </div>
    );
  }

  faculties() {
    return (
      <div
        className={cx(styles.faculties, styles.trigger_right)}
        onClick={() => this.props.functions.setSelector('fac')}
      >
        Faculties & Programs
        <i className={styles.chevron_right} />
      </div>
    );
  }

  moreFilters() {
    const { searchState: { filter: { others } } } = this.props;
    if (!others) return null; // TODO: put status on filter to have loader

    return (
      <div className={styles.more_filters}>
        More filters
        <Checklist
          options={others}
          onSelect={options => this.props.functions.setOthers(options)}
        />
      </div>
    );
  }

  applyButton() {
    return (
      <div className={styles.apply_wrap}>
        <button onClick={() => this.props.functions.searchUniversity()}>Apply</button>
      </div>
    );
  }
}

export default Advanced.components;
