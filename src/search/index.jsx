import { connect } from 'react-redux';
import { Header } from 'src/common/header';
import Router from 'next/router';
import PropTypes from 'prop-types';
import React from 'react';
import UniversityReceiver from 'utils/university/receiver';
import SearchAction from 'utils/search/action';
import SearchReceiver from 'utils/search/receiver';
import MainWrapper from '../common/wrapper';
import Advanced from './advanced';
import FormInput from './formInputHooks';
import Results from './results';


class Search extends React.Component {
  static async getInitialProps(context) {
    const { reduxStore, query, req } = context;
    const { q, advanced, scholarship, expense, selector } = query;
    const { dispatch } = reduxStore;
    if (req && q) {
      await UniversityReceiver.searchUniversity.bind({ dispatch })({ keyword: q });
    }
    return { params: { q, advanced, scholarship, expense, selector } };
  }

  constructor(props) {
    super(props);
    const { q } = props.params;
    this.state = {
      keyword: q
    };
    this._timeout = null;
  }

  componentDidMount() {
    SearchReceiver.fetchFilterAttributes.bind(this)();
  }

  openAdvanced() {
    const { q: keyword, advanced } = this.props.params;
    const isAdvanced = advanced == 'true';
    Router.replace({
      pathname: '/search',
      query: {
        q: keyword,
        advanced: !isAdvanced
      }
    });
  }

  setSelector(type) {
    const { q: keyword, advanced } = this.props.params;
    Router.push({
      pathname: '/search',
      query: {
        q: keyword,
        advanced,
        selector: type
      }
    });
  }

  handleInputChange = (e) => {
    const { dispatch } = this.props;
    const keyword = e.target.value;
    dispatch(SearchAction.setValue('keyword', keyword));
    // this.popupResult.bind(this)(keyword);
  }

  closeAdvanced() {
    const { searchState: { keyword } } = this.props;
    Router.replace({
      pathname: '/search',
      query: {
        q: keyword
      }
    });
  }

  searchUniversity() {
    const { searchState: { keyword } } = this.props;
    if (keyword.length > 0) {
      const { dispatch } = this.props;
      UniversityReceiver.searchUniversity.bind({ dispatch })({ keyword });
    }
    this.closeAdvanced.bind(this)();
  }

  injectFunctions() {
    return {
      onInputChange: this.handleInputChange,
      searchUniversity: this.searchUniversity.bind(this),
      openAdvanced: this.openAdvanced.bind(this),
      setSelector: this.setSelector.bind(this),
      setSortingValue: SearchReceiver.setSortingValue.bind(this),
      setFilterValues: SearchReceiver.setFilterValues.bind(this),
      setLocations: SearchReceiver.setLocations.bind(this),
      setFaculties: SearchReceiver.setFaculties.bind(this),
      setOthers: SearchReceiver.setOthers.bind(this)
    };
  }

  render() {
    const { universityState: { status }, params: { q, advanced } } = this.props;
    const { searchState: { keyword } } = this.props;
    const props = {
      ...this.props,
      keyword,
      functions: this.injectFunctions()
    };

    return (
      <MainWrapper>
        <Header>
          <FormInput {...props} />
        </Header>
        {(status === 'fetched' && q) ? Results({ ...props }) : ''}
        {advanced === 'true' && Advanced({ ...props })}
      </MainWrapper>
    );
  }
}

const mapStateToProps = state => ({
  searchState: state.searchReducer,
  universityState: state.universityReducer
});

Search.propTypes = {
  dispatch: PropTypes.func.isRequired,
  universityState: PropTypes.object.isRequired,
  searchState: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired
};

// const SearchWithData = withData(Search);

export default connect(mapStateToProps)(Search);
// export default connect(mapStateToProps)(SearchWithData);
