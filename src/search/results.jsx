import React from 'react';
import SearchCounter from 'src/common/search/counter';
import ListUniversity from '../common/university/list';

class ResultsComponent {
  static components(props) {
    return new ResultsComponent(props).root();
  }

  constructor(props) {
    const { universityState: { universities } } = props;
    this.universities = universities;
    this.props = props;
  }

  root() {
    const shadow = true;
    const isFetching = this.status === 'fetching';
    return (
      <React.Fragment>
        {this.resultsCounter()}
        {ListUniversity({ list: this.universities })}
      </React.Fragment>
    );
  }

  resultsCounter() {
    return SearchCounter({ count: this.universities.length });
  }
}

export default ResultsComponent.components;
