import React, { useState, useEffect } from 'react';
import { useLazyQuery } from '@apollo/react-hooks';
import Link from 'next/link';
import QUERY from './query';
import styles from './styles.scss';

const onPressEnter = ({ props, e }) => {
  if (e.keyCode === 13) {
    props.functions.searchUniversity();
  }
};

const onInputChange = ({ props, setQuery, isRefetch, e }) => {
  props.functions.onInputChange(e);
  const { value } = e.target;
  setQuery(value);
  isRefetch(false);
};

const SugestionList = ({ data }) => {
  if (!data) return null;
  const { universities } = data;
  if (!universities) return null;
  return universities.map((university) => {
    return (
      <Link
        href={`/university?slug=${university.id}`}
        as={`/university/${university.id}`}
        key={`un-${university.id}`}
      >
        <div className={styles.suggest_item}>{university.name}</div>
      </Link>
    );
  });
};


let _timeout = null;

export default (props) => {
  const { keyword } = props;

  const [query, setQuery] = useState('');
  const [recall, isRefetch] = useState(false);
  let params = { alias_contains: query };
  if (query && query.trim().split(' ').length > 1) {
    params = { name_contains: query };
  }

  const [loadUniversities, { data }] = useLazyQuery(
    QUERY.SEARCH_UNIVERSITY,
    {
      variables: { where: params },
      skip: !recall
    }
  );

  const MINIMUM_CHAR = 2;
  const WAIT_INTERVAL = 700;

  useEffect(() => {
    if (_timeout) window.clearTimeout(_timeout);

    if (query.length >= MINIMUM_CHAR) {
      _timeout = window.setTimeout(() => {
        isRefetch(true);
      }, WAIT_INTERVAL);
    }
  }, [query]);

  useEffect(() => {
    if (recall) {
      loadUniversities();
    }
  }, [recall]);

  return (
    <React.Fragment>
      <div className={styles.form_section}>
        <input
          className={styles.input_form}
          // ref={(input) => { this.searchInput = input; }}
          type="text"
          placeholder="Search for uinversity..."
          onChange={e => onInputChange({ props, e, setQuery, isRefetch })}
          onKeyDown={e => onPressEnter({ props, e })}
          value={keyword}
        />
        <button
          className={styles.button_filter}
          type="button"
          disabled={props.disabled}
          onClick={props.functions.openAdvanced}
        />
      </div>
      <div className={styles.suggestion}>
        <SugestionList data={data} />
      </div>
    </React.Fragment>
  );
};
