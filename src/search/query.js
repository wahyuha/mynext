import { gql } from 'apollo-boost';

const SEARCH_UNIVERSITY = gql`
  query getUniversities($where: JSON!) {
    universities(where: $where)
    {
      id
      name
      alias
      city{
        name
        province{
          name
        }
      }
    }
  }
`;

export default { SEARCH_UNIVERSITY };
