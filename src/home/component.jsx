/* eslint-disable no-else-return */
import { DotsColorful } from 'src/common/loader';
import Link from 'next/link';
import React from 'react';
import Router from 'next/router';
import NewsItem from 'src/common/news';
import ScholarshipsItem from 'src/common/scholarship';
import TopNavbar from 'src/common/topNavbar';
import styles from './styles.scss';

class HomeComponent {
  static components(props) {
    return new HomeComponent(props).root();
  }

  constructor(props) {
    this.props = props;
  }

  root() {
    return (
      <div className={styles.home_wrapper}>
        {/* <TopNavbar /> */}
        {this.headerBoard()}
        {this.featuredUniversities()}
        <div className={styles.home_content}>
          <div className={styles.borders} />
          {this.topScholarships()}
          <div className={styles.borders} />
          {this.enrollment()}
          <div className={styles.borders} />
          {this.topNews()}
        </div>
      </div>
    );
  }

  headerBoard() {
    const randomBg = (Math.floor(Math.random() * 2) + 1).toString();
    return (
      <div className={styles[`header_board_${randomBg}`]}>
        <div className={styles.board_content}>
          <img src="/static/icons/logo.png" alt="Student.id" />
          <h2 className={styles.label_h}>
            <span className={styles.label_left}>CARI</span>
            <span className={styles.label_right}>KAMPUS</span>
          </h2>
          <p>Temukan kampus kamu disini. Pastikan memilih kampus berdasarkan informasi yang relevan.</p>
          {this.searchSection()}
        </div>
      </div>
    );
  }

  searchSection() {
    return (
      <div className={styles.search_section}>
        <div className={styles.search_h}>
          <i />
          <input
            type="text"
            placeholder="Nama kampus"
            onClick={() => Router.push('/search')}
          />
        </div>
        <button>Cari</button>
      </div>
    );
  }

  featuredUniversities() {
    const { universityState: { topHome: { status, data } } } = this.props;
    if (status === 'fetching') {
      return <DotsColorful />;
    } else if (status === 'fetched') {
      return (
        <div className={styles.featured_univ}>
          <div className={styles.univ_header}>
            <h3>Top Universities</h3>
            <Link
              href="/top-universities"
              as="/top-universities"
            >
              See all &gt;
            </Link>
          </div>
          <div className={styles.featured_slider_wrap}>
            <div className={styles.featured_slider}>
              {data.map(univ => this.featuredUnivItem(univ))}
            </div>
          </div>
        </div>
      );
    } else if (data.length <= 0) {
      return null;
    }
    return '';
  }

  featuredUnivItem(university) {
    return (
      <Link
        href={`/university?slug=${university.id}`}
        as={`/university/${university.id}`}
        key={`un-${university.id}`}
      >
        <a href="/top-universities" className={styles.ahref}>
          <div className={styles.featured_wrap}>
            <img src={university.cover.url} alt={university.name} />
          </div>
          <h2>{university.name}</h2>
        </a>
      </Link>
    );
  }

  topScholarships() {
    const { scholarshipState: { topHome: { status, data } } } = this.props;
    if (status === 'fetching') {
      return <DotsColorful />;
    } else if (status === 'fetched') {
      return (
        <React.Fragment>
          <div className={styles.scholar}>
            <div className={styles.scholar_header}>
              <h3>Scholarship Program</h3>
              <Link
                href="/top-scholarships"
                as="/top-scholarships"
              >
                See all &gt;
              </Link>
            </div>
            {data.map(scho => <ScholarshipsItem {...scho} />)}
          </div>
        </React.Fragment>
      );
    } else if (data.length <= 0) {
      return null;
    }
    return '';
  }

  topNews() {
    const { newsState: { topHome: { status, data } } } = this.props;
    if (status === 'fetching') {
      return <DotsColorful />;
    } else if (status === 'fetched') {
      return (
        <React.Fragment>
          <div className={styles.scholar}>
            <div className={styles.scholar_header}>
              <h3>Campus Update</h3>
            </div>
            {data.map(scho => <NewsItem {...scho} />)}
          </div>
        </React.Fragment>
      );
    } else if (data.length <= 0) {
      return null;
    }
    return '';
  }

  enrollment() {
    return (
      <React.Fragment>
        <div className={styles.enroll}>
          <div className={styles.enroll_header}>
            <h3>Enrollment Open Soon</h3>
            <Link
              href="/top-universities"
              as="/top-universities"
            >
              See all &gt;
            </Link>
          </div>
          {this.enrollmentData('Parahyangan University')}
          {this.enrollmentData('Bina Nusantara University')}
        </div>
      </React.Fragment>
    );
  }

  enrollmentData(univ) {
    return (
      <React.Fragment>
        <div className={styles.enroll_list}>
          <div className={styles.enroll_item}>
            <img src="/static/home/univ-bg-3.jpg" alt="ss" />
          </div>
          <div className={styles.enroll_item}>
            <img src="/static/home/univ-bg.jpg" alt="ss" />
          </div>
          <div className={styles.enroll_item}>
            <img src="/static/home/univ-bg-2.jpg" alt="ss" />
          </div>
          <div className={styles.enroll_item}>
            <img src="/static/home/univ-bg.jpg" alt="ss" />
          </div>
        </div>
        <div className={styles.uni_info}>
          <h2>{univ}</h2>
          <h5>Accredited A</h5>
        </div>
        <div className={styles.uni_city_info}>Bandung</div>
        {this.announcement()}
      </React.Fragment>
    );
  }

  announcement() {
    return (
      <div className={styles.announcement}>
        <div className={styles.announce_img}>
          <img src="/static/icons/icon-announcement.svg" alt="announcement" />
        </div>
        Enrollment: June 10 - September 09, 2019
      </div>
    );
  }
}

export default HomeComponent.components;
