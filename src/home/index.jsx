import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';
import UniversityReceiver from 'utils/university/receiver';
import NewsReceiver from 'utils/news/receiver';
import ScholarshipReceiver from 'utils/scholarship/receiver';
import HomeComponent from './component';
import MainWrapper from '../common/wrapper';

class Home extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    UniversityReceiver.topUniversitiesHome.bind({ dispatch })();
    ScholarshipReceiver.topScholarshipHome.bind({ dispatch })();
    NewsReceiver.topNewsHome.bind({ dispatch })();
  }

  render() {
    return (
      <MainWrapper>
        {HomeComponent({ ...this.props })}
      </MainWrapper>
    );
  }
}

const mapStateToProps = state => ({
  newsState: state.newsReducer,
  universityState: state.universityReducer,
  scholarshipState: state.scholarshipReducer
});

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  newsState: PropTypes.object.isRequired,
  universityState: PropTypes.object.isRequired,
  scholarshipState: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(Home);
