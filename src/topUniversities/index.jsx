import { connect } from 'react-redux';
import { LoaderFull } from 'src/common/loader';
import PropTypes from 'prop-types';
import React from 'react';
import UniversityReceiver from 'utils/university/receiver';
import TopUniversitiesComponent from './component';
import MainWrapper from '../common/wrapper';

class TopUniversities extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    UniversityReceiver.topUniversities.bind({ dispatch })();
  }

  render() {
    const { universityState } = this.props;
    if (universityState.status === 'fetched') {
      return (
        <MainWrapper>
          {TopUniversitiesComponent({ ...this.props })}
        </MainWrapper>
      );
    }
    return (
      <MainWrapper>
        {LoaderFull('Fetching universities ..')}
      </MainWrapper>
    );
  }
}

const mapStateToProps = state => ({
  universityState: state.universityReducer
});

TopUniversities.propTypes = {
  dispatch: PropTypes.func.isRequired,
  universityState: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(TopUniversities);
