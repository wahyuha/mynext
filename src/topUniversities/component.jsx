import React from 'react';
import Router from 'next/router';
import { Header } from 'src/common/header';
import SearchInputTrigger from 'src/common/search/trigger';
import ListUniversity from '../common/university/list';
import styles from './styles.scss';

class TopUniversitiesComponent {
  static components(props) {
    return new TopUniversitiesComponent(props).root();
  }

  constructor(props) {
    const { universityState: { universities } } = props;
    this.universities = universities;
    this.props = props;
  }

  root() {
    return (
      <React.Fragment>
        <Header title="Top University" />
        <div className={styles.main_board_wrap}>
          <SearchInputTrigger
            onFocus={() => Router.push('/search')}
          />
        </div>
        {ListUniversity({ list: this.universities })}
      </React.Fragment>
    );
  }
}

export default TopUniversitiesComponent.components;
