import TopUniversities from 'src/topUniversities';

function TopUniversitiesPage() {
  return <TopUniversities />;
}

export default TopUniversitiesPage;
