import { ApolloProvider } from '@apollo/react-hooks';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import React from 'react';
import withApollo from 'utils/base/withApollo';
import withReduxStore from 'utils/base/withReduxStore';


class Main extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    /* your own logic */
    return { pageProps };
  }

  render() {
    const { Component, pageProps, reduxStore, apollo } = this.props;
    return (
      <Container>
        <ApolloProvider client={apollo}>
          <Provider store={reduxStore}>
            <Component {...pageProps} />
          </Provider>
        </ApolloProvider>
      </Container>
    );
  }
}

const MainWithApollo = withApollo(Main);

export default withReduxStore(MainWithApollo);
