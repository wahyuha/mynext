import BaseAction from '../base/action';
import ReduxUtil from '../base/redux';
import Constants from './constants';

class ScholarshipAction {
  static setScholarships(values) {
    return { type: 'SET_SCHOLARSHIPS', values };
  }

  static setTopHome(values, status) {
    return { type: 'SET_TOP_HOME', values, status };
  }

  static setScholarshipDetail(value) {
    return { type: 'SET_SCHOLARSHIP_DETAIL', value };
  }

  static setStatusDetail(status) {
    return { type: 'SET_STATUS_DETAIL', status };
  }
}

ScholarshipAction.namespace = Constants.NAMESPACE;

export default ReduxUtil.extend(ScholarshipAction).with(BaseAction);
