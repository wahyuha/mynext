import BaseAPI from '../base/api';

export default class ScholarshipAPI extends BaseAPI {
  static async getBySlug({ slug }) {
    return this.executeGET(`scholarships?id=${slug}`);
  }

  static async getTopScholarships() {
    return this.executeGET('scholarships?topscholarship_gt=0');
  }

  static async topScholarshipHome() {
    return this.executeGET('topscholarships');
  }
}
