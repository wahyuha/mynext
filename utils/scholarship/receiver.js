import ScholarshipAction from './action';
import ScholarshipAPI from './api';

export default class ScholarshipReceiver {
  static async topScholarshipHome() {
    const { dispatch } = this;
    dispatch(ScholarshipAction.setTopHome('', 'fetching'));
    try {
      const response = await ScholarshipAPI.topScholarshipHome();
      const [result] = response;
      if (result) {
        dispatch(ScholarshipAction.setTopHome(result.scholarships, 'fetched'));
      } else {
        dispatch(ScholarshipAction.setTopHome('', 'failed'));
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getScholarships() {
    const { dispatch } = this;
    dispatch(ScholarshipAction.setStatus('fetching'));
    dispatch(ScholarshipAction.setScholarships(''));
    try {
      const response = await ScholarshipAPI.getTopScholarships();
      if (response) {
        dispatch(ScholarshipAction.setScholarships(response));
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getScholarshipDetail(slug) {
    const { dispatch } = this;
    dispatch(ScholarshipAction.setStatus('fetching'));
    dispatch(ScholarshipAction.setScholarshipDetail(''));
    try {
      const response = await ScholarshipAPI.getBySlug({ slug });
      if (response) {
        dispatch(ScholarshipAction.setScholarshipDetail(response[0]));
      }
    } catch (error) {
      console.log(error);
    }
  }
}
