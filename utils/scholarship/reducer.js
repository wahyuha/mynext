import clone from 'clone';
import BaseReducer from '../base/reducers';
import Constants from './constants';

class ScholarshipReducer extends BaseReducer {
  defaultConstant() {
    return Constants.NAMESPACE;
  }

  defaultState() {
    return {
      status: '',
      scholarships: '',
      detail: {
        status: '',
        data: ''
      },
      errorMessage: '',
      topHome: {
        status: '',
        data: ''
      }
    };
  }

  setScholarships({ state, values }) {
    const newState = clone(state);
    newState.status = values ? 'fetched' : 'fetching';
    newState.scholarships = values;
    return newState;
  }

  setTopHome({ state, values, status }) {
    const newState = clone(state);
    const data = {
      status,
      data: values
    };
    newState.topHome = data;
    return newState;
  }

  setStatusDetail({ state, status }) {
    const newState = clone(state);
    newState.detail = {
      ...newState.detail,
      status
    };
    return newState;
  }

  setScholarshipDetail({ state, value }) {
    const newState = clone(state);
    newState.detail = {
      status: value ? 'fetched' : 'fetching',
      data: value
    };
    return newState;
  }

  addBaseCases() {
    super
      .addBaseCases()
      .addCase('SET_SCHOLARSHIPS', this.setScholarships)
      .addCase('SET_TOP_HOME', this.setTopHome)
      .addCase('SET_STATUS_DETAIL', this.setStatusDetail)
      .addCase('SET_SCHOLARSHIP_DETAIL', this.setScholarshipDetail);
    return this;
  }
}

const scholarshipReducer = new ScholarshipReducer().reducer();

export default scholarshipReducer;
