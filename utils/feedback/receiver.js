import AppConstants from 'utils/base/appConstants';
import UniversityAction from '../university/action';
import FeedbackAPI from './api';

export default class FeedbackReceiver {
  static async fetchFeedback(slug) {
    const { dispatch } = this;

    dispatch(UniversityAction.setStatusFeedback('fetching'));
    try {
      const response = await FeedbackAPI.fetchByUniversitySlug({ slug });
      dispatch(UniversityAction.setUniversityFeedback(response));
    } catch (error) {
      console.log(error);
      dispatch(UniversityAction.setStatusFeedback('error'));
    }
  }
}
