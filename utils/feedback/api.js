import BaseAPI from '../base/api';

export default class FeedbackAPI extends BaseAPI {
  static async fetchByUniversitySlug({ slug }) {
    return this.executeGET(`feedback/?ordering=-created_at&university=${slug}`);
  }
}
