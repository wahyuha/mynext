import clone from 'clone';
import BaseReducer from '../base/reducers';
import Constants from './constants';

class NewsReducer extends BaseReducer {
  defaultConstant() {
    return Constants.NAMESPACE;
  }

  defaultState() {
    return {
      status: '',
      news: '',
      detail: {
        status: '',
        data: ''
      },
      errorMessage: '',
      topHome: {
        status: '',
        data: ''
      }
    };
  }

  setNews({ state, values }) {
    const newState = clone(state);
    newState.status = values ? 'fetched' : 'fetching';
    newState.news = values;
    return newState;
  }

  setTopHome({ state, values, status }) {
    const newState = clone(state);
    const data = {
      status,
      data: values
    };
    newState.topHome = data;
    return newState;
  }

  setStatusDetail({ state, status }) {
    const newState = clone(state);
    newState.detail = {
      ...newState.detail,
      status
    };
    return newState;
  }

  setNewsDetail({ state, value }) {
    const newState = clone(state);
    newState.detail = {
      status: value ? 'fetched' : 'fetching',
      data: value
    };
    return newState;
  }

  addBaseCases() {
    super
      .addBaseCases()
      .addCase('SET_NEWS', this.setNews)
      .addCase('SET_TOP_HOME', this.setTopHome)
      .addCase('SET_STATUS_DETAIL', this.setStatusDetail)
      .addCase('SET_NEWS_DETAIL', this.setNewsDetail);
    return this;
  }
}

const newsReducer = new NewsReducer().reducer();

export default newsReducer;
