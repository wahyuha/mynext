import NewsAction from './action';
import NewsAPI from './api';

export default class NewsReceiver {
  static async topNewsHome() {
    const { dispatch } = this;
    dispatch(NewsAction.setTopHome('', 'fetching'));
    try {
      const response = await NewsAPI.topNewsHome();
      const [result] = response;
      if (result) {
        dispatch(NewsAction.setTopHome(result.news, 'fetched'));
      } else {
        dispatch(NewsAction.setTopHome('', 'failed'));
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getNews() {
    const { dispatch } = this;
    dispatch(NewsAction.setStatus('fetching'));
    dispatch(NewsAction.setNewss(''));
    try {
      const response = await NewsAPI.getTopNewss();
      if (response) {
        dispatch(NewsAction.setNews(response));
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getNewsDetail(slug) {
    const { dispatch } = this;
    dispatch(NewsAction.setStatus('fetching'));
    dispatch(NewsAction.setNewsDetail(''));
    try {
      const response = await NewsAPI.getBySlug({ slug });
      if (response) {
        dispatch(NewsAction.setNewsDetail(response[0]));
      }
    } catch (error) {
      console.log(error);
    }
  }
}
