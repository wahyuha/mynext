import BaseAction from '../base/action';
import ReduxUtil from '../base/redux';
import Constants from './constants';

class NewsAction {
  static setNews(values) {
    return { type: 'SET_NEWS', values };
  }

  static setTopHome(values, status) {
    return { type: 'SET_TOP_HOME', values, status };
  }

  static setNewsDetail(value) {
    return { type: 'SET_NEWS_DETAIL', value };
  }

  static setStatusDetail(status) {
    return { type: 'SET_STATUS_DETAIL', status };
  }
}

NewsAction.namespace = Constants.NAMESPACE;

export default ReduxUtil.extend(NewsAction).with(BaseAction);
