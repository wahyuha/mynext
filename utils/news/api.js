import BaseAPI from '../base/api';

export default class NewsAPI extends BaseAPI {
  static async getBySlug({ slug }) {
    return this.executeGET(`news?id=${slug}`);
  }

  static async getTopNews() {
    return this.executeGET('news?topnews_gt=0');
  }

  static async topNewsHome() {
    return this.executeGET('topnews');
  }
}
