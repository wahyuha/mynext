import clone from 'clone';
import BaseReducer from '../base/reducers';
import Constants from './constants';

class SearchReducer extends BaseReducer {
  defaultConstant() {
    return Constants.NAMESPACE;
  }

  defaultState() {
    return {
      keyword: '',
      status: '',
      results: '',
      sortBy: '',
      filter: {
        fee: '',
        rating: '',
        locations: '',
        faculties: '',
        others: ''
      }
    };
  }

  searchResults({ state, results }) {
    const newState = clone(state);
    newState.status = 'fetched';
    newState.universities = results;
    return newState;
  }

  setSorting({ state, sortBy }) {
    const newState = clone(state);
    newState.sortBy = sortBy;
    return newState;
  }

  setFilter({ state, fee, rating }) {
    const newState = clone(state);
    const newFilter = newState.filter;
    if (fee) newFilter.fee = fee;
    if (rating) newFilter.rating = rating;

    newState.filter = newFilter;
    return newState;
  }

  setFilterLocations({ state, locations }) {
    const newState = clone(state);
    newState.filter = {
      ...newState.filter,
      locations
    };

    return newState;
  }

  setFilterFaculties({ state, faculties }) {
    const newState = clone(state);
    newState.filter = {
      ...newState.filter,
      faculties
    };

    return newState;
  }

  setFilterOthers({ state, others }) {
    const newState = clone(state);
    newState.filter = {
      ...newState.filter,
      others
    };

    return newState;
  }

  addBaseCases() {
    super
      .addBaseCases()
      .addCase('SET_RESULTS', this.searchResults)
      .addCase('SET_SORTING', this.setSorting)
      .addCase('SET_FILTER_LOCATIONS', this.setFilterLocations)
      .addCase('SET_FILTER_FACULTIES', this.setFilterFaculties)
      .addCase('SET_FILTER_OTHERS', this.setFilterOthers)
      .addCase('SET_FILTER', this.setFilter);
    return this;
  }
}

const searchReducer = new SearchReducer().reducer();

export default searchReducer;
