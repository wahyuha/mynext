import BaseAction from '../base/action';
import ReduxUtil from '../base/redux';
import Constants from './constants';

class SearchAction {
  static searchResults(results) {
    return { type: 'SET_RESULTS', results };
  }

  static setSorting(sortBy) {
    return { type: 'SET_SORTING', sortBy };
  }

  static setFilter({ fee, rating, locations, faculties, hasScholarship }) {
    return { type: 'SET_FILTER', fee, rating, locations, faculties, hasScholarship };
  }

  static setFilterLocations(locations) {
    return { type: 'SET_FILTER_LOCATIONS', locations };
  }

  static setFilterFaculties(faculties) {
    return { type: 'SET_FILTER_FACULTIES', faculties };
  }

  static setFilterOthers(others) {
    return { type: 'SET_FILTER_OTHERS', others };
  }
}

SearchAction.namespace = Constants.NAMESPACE;

export default ReduxUtil.extend(SearchAction).with(BaseAction);
