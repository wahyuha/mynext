import filterLocationsJson from './filterJson/locations.json';
import filterFacultiesJson from './filterJson/faculties.json';
import filterOthersJson from './filterJson/others.json';
import SearchAction from './action';

class SearchReceiver {
  static fetchFilterAttributes() {
    const { dispatch, searchState: { filter: { locations, faculties, others } } } = this.props;

    const locationsFixed = SearchReceiver.addDefaultSelectedttributes(filterLocationsJson);
    const facultiesFixed = SearchReceiver.addDefaultSelectedttributes(filterFacultiesJson);
    const othersFixed = SearchReceiver.addDefaultSelectedttributes(filterOthersJson);
    !locations && dispatch(SearchAction.setFilterLocations(locationsFixed));
    !faculties && dispatch(SearchAction.setFilterFaculties(facultiesFixed));
    !others && dispatch(SearchAction.setFilterOthers(othersFixed));
  }

  static addDefaultSelectedttributes(options) {
    return options.map((option) => {
      return ({
        ...option,
        selected: option.selected || false
      });
    });
  }

  static setSortingValue(value) {
    const { dispatch } = this.props;
    dispatch(SearchAction.setSorting(value));
  }

  static setFilterValues({ fee, rating }) {
    const { dispatch } = this.props;
    dispatch(SearchAction.setFilter({ fee, rating }));
  }

  static setLocations(options) {
    // const strings = SearchReceiver.objectToStringValue(options);
    this.props.dispatch(SearchAction.setFilterLocations(options));
  }

  static setFaculties(options) {
    // const strings = SearchReceiver.objectToStringValue(options);
    this.props.dispatch(SearchAction.setFilterFaculties(options));
  }

  static setOthers(options) {
    // const strings = SearchReceiver.objectToStringValue(options);
    this.props.dispatch(SearchAction.setFilterOthers(options));
  }

  static objectToStringValue(obj) {
    let concatedString = '';
    const selectedValues = obj.filter(value => value.selected);
    concatedString = Object.keys(selectedValues)
      .reduce((res, v) => res.concat(selectedValues[v].value), []).join();

    return concatedString;
  }
}

export default SearchReceiver;
