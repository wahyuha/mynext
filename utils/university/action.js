import BaseAction from '../base/action';
import ReduxUtil from '../base/redux';
import Constants from './constants';

class UniversityAction {
  static setUniversities(values) {
    return { type: 'SET_UNIVERSITIES', values };
  }

  static setTopHome(values, status) {
    return { type: 'SET_TOP_HOME', values, status };
  }

  static setUniversityDetail(value) {
    return { type: 'SET_UNIVERSITY_DETAIL', value };
  }

  static setStatusDetail(status) {
    return { type: 'SET_DETAIL_DETAIL', status };
  }

  static setUniversityFeedback(value) {
    return { type: 'SET_UNIVERSITY_FEEDBACK', value };
  }

  static setStatusFeedback(status) {
    return { type: 'SET_STATUS_FEEDBACK', status };
  }
}

UniversityAction.namespace = Constants.NAMESPACE;

export default ReduxUtil.extend(UniversityAction).with(BaseAction);
