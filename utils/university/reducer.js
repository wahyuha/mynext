import clone from 'clone';
import BaseReducer from '../base/reducers';
import Constants from './constants';

class UniversityReducer extends BaseReducer {
  defaultConstant() {
    return Constants.NAMESPACE;
  }

  defaultState() {
    return {
      status: '',
      universities: '',
      detail: {
        status: '',
        data: ''
      },
      errorMessage: '',
      feedback: {
        status: '',
        data: ''
      },
      topHome: {
        status: '',
        data: ''
      }
    };
  }

  setUniversities({ state, values }) {
    const newState = clone(state);
    newState.universities = values;
    return newState;
  }

  setTopHome({ state, values, status }) {
    const newState = clone(state);
    const data = {
      status,
      data: values
    };
    newState.topHome = data;
    return newState;
  }

  setStatusDetail({ state, status }) {
    const newState = clone(state);
    newState.detail = {
      ...newState.detail,
      status
    };
    return newState;
  }
  
  setStatusFeedback({ state, status }) {
    const newState = clone(state);
    newState.feedback = {
      ...newState.feedback,
      status
    };
    return newState;
  }

  setUniversityDetail({ state, value }) {
    const newState = clone(state);
    newState.detail = {
      status: 'fetched',
      data: value
    };
    return newState;
  }

  setUniversityFeedback({ state, value }) {
    const newState = clone(state);
    newState.feedback = {
      status: 'fetched',
      data: value
    };
    return newState;
  }

  addBaseCases() {
    super
      .addBaseCases()
      .addCase('SET_UNIVERSITIES', this.setUniversities)
      .addCase('SET_TOP_HOME', this.setTopHome)
      .addCase('SET_STATUS_DETAIL', this.setStatusDetail)
      .addCase('SET_STATUS_FEEDBACK', this.setStatusFeedback)
      .addCase('SET_UNIVERSITY_DETAIL', this.setUniversityDetail)
      .addCase('SET_UNIVERSITY_FEEDBACK', this.setUniversityFeedback);
    return this;
  }
}

const universityReducer = new UniversityReducer().reducer();

export default universityReducer;
