import AppConstants from 'utils/base/appConstants';
import UniversityAction from './action';
import UniversityAPI from './api';
// import { quickSearch } from './apiGraphql';


export default class UniversityReceiver {
  static async fetchUniversities() {
    const { dispatch } = this;
    dispatch(UniversityAction.setStatus('fetching'));
    try {
      const response = await UniversityAPI.fetchUniversities({ countryId: AppConstants.countyId });
      dispatch(UniversityAction.setUniversities(response));
      dispatch(UniversityAction.setStatus('fetched'));
    } catch (error) {
      console.log(error);
      dispatch(UniversityAction.setStatus('error'));
    }
  }

  static async topUniversities() {
    const { dispatch } = this;
    dispatch(UniversityAction.setStatus('fetching'));
    try {
      const response = await UniversityAPI.topUniversities();
      dispatch(UniversityAction.setUniversities(response));
      dispatch(UniversityAction.setStatus('fetched'));
    } catch (error) {
      console.log(error);
      dispatch(UniversityAction.setStatus('error'));
    }
  }

  static async topUniversitiesHome() {
    const { dispatch } = this;
    dispatch(UniversityAction.setTopHome('', 'fetching'));
    try {
      const response = await UniversityAPI.topUniversitiesHome();
      const [result] = response;
      if (result) {
        dispatch(UniversityAction.setTopHome(result.universities, 'fetched'));
      } else {
        dispatch(UniversityAction.setTopHome('', 'failed'));
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getUniversityDetail(slug) {
    const { dispatch, universityState } = this;
    if (universityState) {
      if (universityState.detail.status === 'fetched'
        && universityState.detail.id === slug) return; // not proven yet
    }

    dispatch(UniversityAction.setStatusDetail('fetching'));
    try {
      const response = await UniversityAPI.getBySlug({ slug });
      dispatch(UniversityAction.setUniversityDetail(response));
    } catch (error) {
      console.log(error);
      dispatch(UniversityAction.setStatusDetail('error'));
    }
  }

  static async searchUniversity({ keyword }) {
    const { dispatch } = this;
    dispatch(UniversityAction.setStatus('fetching'));
    try {
      const response = await UniversityAPI.search({ keyword });
      dispatch(UniversityAction.setUniversities(response));
      dispatch(UniversityAction.setStatus('fetched'));
    } catch (error) {
      console.log(error);
      dispatch(UniversityAction.setStatus('error'));
    }
  }

  static resetUniversity() {
    const { dispatch } = this;
    dispatch(UniversityAction.setStatus(''));
    dispatch(UniversityAction.setUniversities(''));
  }
}
