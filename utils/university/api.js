import BaseAPI from '../base/api';

export default class UniversityAPI extends BaseAPI {
  static async fetchUniversities({ countryId }) {
    return this.executeGET(`universities/?country=${countryId}`);
  }

  static async getBySlug({ slug }) {
    return this.executeGET(`universities/${slug}`);
  }

  static async search({ keyword }) {
    const params = {
      name_contains: keyword
    };
    return this.executeGET('universities/', params);
  }

  static async topUniversities() {
    return this.executeGET('universities?topuniversity_gt=0');
  }

  static async topUniversitiesHome() {
    return this.executeGET('topuniversities');
  }
}
