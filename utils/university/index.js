import _ from 'lodash';

export default class UniversityHelper {
  static getExpenseStatus(university) {
    const { expense } = university;
    const expObj = {};
    if (expense === 'LOW') {
      expObj.label = 'Low';
      expObj.number = 1;
    } else if (expense === 'MEDIUM') {
      expObj.label = 'Medium';
      expObj.number = 1;
    } else if (expense === 'HIGH') {
      expObj.label = 'High';
      expObj.number = 3;
    }
    return expObj;
  }

  static facultiesByDegree(faculties) {
    const grouped = _.groupBy(faculties, fac => fac.degree);
    const combined = { ALL: faculties, ...grouped };
    return combined;
  }

  static facultiesByParent(faculties) {
    const grouped = _.groupBy(faculties, item => item.faculty.name);
    return grouped;
  }
}
