import { combineReducers } from 'redux';
import newsReducer from 'utils/news/reducer';
import universityReducer from 'utils/university/reducer';
import searchReducer from 'utils/search/reducer';
import scholarshipReducer from 'utils/scholarship/reducer';

const reducers = combineReducers({
  newsReducer,
  searchReducer,
  universityReducer,
  scholarshipReducer
});

export default reducers;
