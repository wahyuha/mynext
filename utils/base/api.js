import Cookies from 'js-cookie';
import Axios from 'axios';
import getConfig from 'next/config';

export default class BaseAPI {
  constructor() {
    this._timeout = 30000;
    this._userToken = Cookies.get('im_token') || '';
    this.status;
    this.createAPI();
  }

  static async executeGET(params, getParams, token) {
    const newParams = this.createAccessToken(params, token);
    // TODO: need better implementation to get token cookie in SSR
    try {
      const response = await new BaseAPI().axios.get(newParams, { params: getParams });
      this.status = response.status;
      const { data } = response;
      return data;
    } catch (e) {
      console.log('error', e); // TODO: create logger
      return null;
    }
  }

  static async executePOST(params, payload) {
    try {
      const response = await new BaseAPI().axios.post(params, payload);
      this.status = response.status;
      const { data } = response;
      return data;
    } catch (e) {
      console.log('error', e); // TODO: create logger
      return null;
    }
  }

  static async executePUT(params, payload) {
    try {
      const response = await new BaseAPI().axios.put(params, payload);
      this.status = response.status;
      const { data } = response;
      return data;
    } catch (e) {
      console.log('error', e); // TODO: create logger
      return null;
    }
  }

  static createAccessToken(params, token) {
    if (!token) return params;
    const operator = params.includes('?') ? '&' : '?';
    return `${params}${operator}access_token=${token}`;
  }

  headers() {
    const headers = {
      Authorization: this._userToken
    };
    return headers;
  }

  createAPI() {
    const { publicRuntimeConfig } = getConfig();
    const headers = this.headers();
    this.axios = Axios.create({
      baseURL: publicRuntimeConfig.BASE_API_URL,
      timeout: this._timeout,
      headers
    });
    return this;
  }
}
