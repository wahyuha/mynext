import ApolloClient, { InMemoryCache } from 'apollo-boost';
import getConfig from 'next/config';
import withApollo from 'next-with-apollo';

const { publicRuntimeConfig } = getConfig();

export default withApollo(
  ({ ctx, headers, initialState }) => new ApolloClient({
    uri: `${publicRuntimeConfig.BASE_API_URL}graphql`,
    cache: new InMemoryCache().restore(initialState || {})
  })
);
