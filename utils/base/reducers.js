import clone from 'clone';

export default class BaseReducer {
  constructor() {
    this.CONSTANT_NAMESPACE = this.defaultConstant();
    this.callbacks = {};
  }

  /** @override */
  defaultConstant() {
    return '';
  }

  /** @override */
  defaultState() {
    return {};
  }

  reducer() {
    this.addBaseCases();
    return this.dynamicSwitchReducer();
  }

  dynamicSwitchReducer() {
    return (state = this.defaultState(), action) => {
      return this.dynamicSwitch({ state, action });
    };
  }

  dynamicSwitch({ state, action }) {
    if (this.callbacks[action.type]) {
      return this.callbacks[action.type]({ state, ...action });
    }
    return state;
  }

  /** @override */
  addBaseCases() {
    this
      .addCase('DEFAULT_STATE', this.defaultState)
      .addCase('SET_STATUS', this.setStatus)
      .addCase('SET_VALUE', this.setValue)
      .addCase('SET_STATE', this.setValues);
    return this;
  }

  addCase(constant, callback) {
    this.callbacks[this.constant(constant)] = callback.bind(this);
    return this;
  }

  constant(value) {
    return `${this.CONSTANT_NAMESPACE}_${value}`;
  }

  setStatus({ state, status }) {
    const newState = clone(state);
    newState.status = status;
    return newState;
  }

  setValue({ state, key, value }) {
    const newState = clone(state);
    newState[key] = value;
    return newState;
  }

  setValues({ state, states: changedStates }) {
    const newState = clone(state);
    Object.keys(changedStates).forEach((stateKey) => {
      const stateValue = changedStates[stateKey];
      newState[stateKey] = stateValue;
    });
    return newState;
  }
}
