import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import getConfig from 'next/config';
import reducers from './combineReducer';

export function initializeStore(initialState = {}) {
  const middleware = [];
  const { publicRuntimeConfig: { NODE_ENV } } = getConfig();
  let appliedMiddleware = applyMiddleware(...middleware);
  if (NODE_ENV === 'development') {
    appliedMiddleware = composeWithDevTools();
  }

  return createStore(
    reducers,
    initialState,
    appliedMiddleware
  );
}
