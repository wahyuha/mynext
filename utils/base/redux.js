import clone from 'clone';

export default class ReduxUtil {
  static extend(ChildClass) {
    return {
      with(ParentClass) {
        return new ReduxUtil({ ChildClass, ParentClass }).extendStaticActions();
      }
    };
  }

  constructor(parameters) {
    this.ChildClass = parameters.ChildClass;
    this.ParentClass = parameters.ParentClass;
    this.namespace = this.ChildClass.namespace;
    this.ExtendedClass = {};
  }

  extendStaticActions() {
    this.createActionsWithNamespace(this.ParentClass);
    this.createActionsWithNamespace(this.ChildClass);
    return this.ExtendedClass;
  }

  createActionsWithNamespace(ActionClass) {
    const staticFunctions = Object.getOwnPropertyNames(ActionClass);
    staticFunctions.forEach((functionName) => {
      try {
        if (typeof ActionClass[functionName] !== 'function') {
          return;
        }
        const action = ActionClass[functionName]({});
        if (!action.type) {
          return;
        }

        this.ExtendedClass[functionName] = (...args) => {
          const actionObject = ActionClass[functionName](...args);
          const actionObjectWithNamespace = clone(actionObject);
          actionObjectWithNamespace.type = `${this.namespace}_${actionObject.type}`;
          return actionObjectWithNamespace;
        };
      } catch (e) {}
    });

    return this;
  }
}
