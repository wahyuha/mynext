export default class BaseAction {
  static setValue(key, value) {
    return { type: 'SET_VALUE', key, value };
  }

  static setStatus(status) {
    return { type: 'SET_STATUS', status };
  }

  static defaultState() {
    return { type: 'DEFAULT_STATE' };
  }
}
