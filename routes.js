const nextRoutes = require('next-routes');

const routes = (module.exports = nextRoutes());

routes.add('news', '/news/:slug');
routes.add('university', '/university/:slug');
routes.add('scholarship', '/scholarship/:slug');
routes.add('top-scholarships', '/top-scholarship');
routes.add('search', '/search/');
