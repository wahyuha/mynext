const { parsed: env } = require('dotenv').config();
const Dotenv = require('dotenv-webpack');
const path = require('path');
const withPlugins = require('next-compose-plugins');
const images = require('next-images');
const withFonts = require('next-fonts');
const sassConfig = require('./config/sassConfig');
const alias = require('./config/alias');

const publicRuntimeConfig = { ...env };
const isDev = process.env.NODE_ENV !== 'production';

module.exports = withPlugins([
  alias,
  sassConfig,
  images,
  withFonts
],
{
  publicRuntimeConfig,
  webpack: (config) => {
    if (isDev) {
      config.plugins.push(
        new Dotenv({
          path: path.join(__dirname, '.env'),
          systemvars: true
        })
      );
    }

    return config;
  }
});
