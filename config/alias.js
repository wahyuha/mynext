// const path = require('path');

module.exports = {
  webpack: (config) => {
    config.resolve.alias = {
      ...(config.resolve.alias || {})
      // components: path.join(__dirname, 'components'),
      // utils: path.join(__dirname, 'utils')
    };
    return config;
  }
};
